// ============================================================
// Import modules
import Channel from './Channel';

// ============================================================
// Module's constants and variables

/**
 * List of socket connection event types
 * @enum {string}
 * @readonly
 * @public
 */
const SocketConnectionEvent = {
    /**
     * A channel is deleted
     * @see SocketConnectionEvent:deleteChannel
     */
    deleteChannel: 'delete-channel',

    /**
     * A channel has been focused
     * @see SocketConnectionEvent:focus
     */
    focus: 'focus',

    /**
     * @see SocketConnectionEvent:message
     */
    message: 'message',

    /**
     * A new channel is created
     * @see SocketConnectionEvent:newChannel
     */
    newChannel: 'new-channel',

    /**
     * The pointer is disabled
     * @see SocketConnectionEvent:pointerDisabled
     */
    pointerDisabled: 'pointer-disabled',

    /**
     * The pointer move
     * @see SocketConnectionEvent:pointerMove
     */
    pointerMove: 'pointer-move',
};

const MessageType = {
    channel: 'channel',
    control: 'control',
    focus: 'focus',
    globalMetadata: 'globalmetadata',
    pointer: 'pointer',
};

const OperationType = {
    create: 'create',
    delete: 'delete',
    focus: 'focus',
};

// ============================================================
// Class
class SocketConnection extends EventTarget {
    /**
     * List of channels
     * @type {Channel[]}
     */
    #channels = [];

    /**
     * The channel currently focused
     * @type {Channel}
     */
    #focusedChannel;

    /**
     * Last message ID
     * @type {number}
     */
    #lastMessageId = 0;

    /**
     * @type {number}
     */
    #maxChannelId = 0;

    /**
     * Return pointer position
     * @type {ChannelPointer}
     */
    #pointer;

    /**
     * @type {boolean}
     */
    #pointerEnabled = false;

    /**
     * Indicate if the current user is a presenter or not
     * @type {boolean}
     */
    #presenter;

    /**
     * List of all subscribers
     * @type {Object.<{onControl: function, onMessage: function}>}
     */
    #subscriptions = {};

    /**
     * URL Of the websocket server
     * @type {WebSocket}
     */
    #ws;

    constructor(wsurl, isPresenter) {
        super();
        this.#ws = new WebSocket(wsurl);
        this.#ws.binaryType = 'arraybuffer';

        this.#presenter = isPresenter;

        this.initialize();
    }

    /**
     * Create a new channel
     * @param {ChannelType} type
     * @param {*} initialState
     * @public
     */
    createChannel(type, initialState) {
        this.#maxChannelId += 1;
        this.sendJson({
            type: MessageType.control,
            op: OperationType.create,
            chanType: type,
            init: initialState,
            scene: 1,
        });
    }

    /**
     * Delete a channel.
     *
     * @param {Object} message
     * @fires SocketConnection:deleteChannel
     * @private
     */
    deleteChannel(id) {
        const channel = this.getChannel(id);
        const index = this.#channels.indexOf(channel);

        if (index < 0) {
            throw new Error(`Channel "${id}" doesn't exists`);
        }

        this.#channels = [
            ...this.#channels.slice(0, index),
            ...this.#channels.slice(index + 1),
        ];

        const subscriptions = this.#subscriptions[id];
        delete this.#subscriptions[id];

        if (channel === this.#focusedChannel) {
            this.#focusedChannel = undefined;
        }

        // Emit event
        const event = new CustomEvent(
            SocketConnectionEvent.deleteChannel,
            {
                detail: {
                    channel,
                    id,
                },
            },
        );

        // Notifying all subscribers of the delete
        subscriptions.forEach(({ onControl }) => {
            if (!onControl) {
                return;
            }

            onControl(event);
        });
        this.dispatchEvent(event);

        if (this.#focusedChannel === undefined) {
            [this.#focusedChannel] = this.#channels;
            this.fireFocusEvent();
        }
    }

    /**
     * @public
     */
    enablePointer() {
        this.#pointerEnabled = true;
    }

    /**
     * @public
     */
    disablePointer() {
        this.#pointerEnabled = false;
    }

    /**
     * @public
     */
    togglePointer(enable) {
        this.#pointerEnabled = enable !== undefined
            ? enable
            : !this.#pointerEnabled;
    }

    /**
     * Return a channel.
     * @param {Channel} channelId - ID of the channel to return
     * @public
     */
    getChannel(channelId) {
        return this.#channels.find(({ id }) => id === channelId);
    }

    /**
     * Return the channel currently focused
     * @returns {Channel}
     * @public
     */
    getFocusedChannel() {
        return this.#focusedChannel;
    }

    /**
     * @returns {ChannelPointer}
     * @public
     */
    getPointer() {
        return this.#pointer;
    }

    /**
     * Initialize the socket connection.
     *
     * @fires SocketConnection:createChannel
     * @fires SocketConnection:deleteChannel
     * @private
     */
    initialize() {
        if (this.isPresenter()) {
            this.#ws.onopen = () => {
                this.sendJson({
                    type: 'hello',
                    presenter: true,
                });
            };
        }

        this.#ws.onmessage = (message) => {
            const id = this.#lastMessageId;
            this.#lastMessageId += 1;

            const data = JSON.parse(message.data);

            data.messageId = id;
            data.timestamp = new Date(data.timestamp);

            switch (data.type) {
            case MessageType.channel:
                this.onChannelMessage(data);
                break;

            case MessageType.control:
                this.onControlMessage(data);
                break;

            case MessageType.focus:
                this.onFocusMessage(data);
                break;

            case MessageType.globalMetadata:
                this.onGlobalMetadataMessage(data);
                break;

            case MessageType.pointer:
                this.onPointerMove(data);
                break;

            default:
                // eslint-disable-next-line no-console
                console.log(data);
                throw new Error(`Unknown message type received: "${data.type}"`);
            }
        };
    }

    /**
     * Indicate if the current user is a presenter or not.
     * @returns {boolean}
     * @public
     */
    isPresenter() {
        return this.#presenter;
    }

    /**
     *
     * @param {boolean} [enable]
     * @public
     */
    togglePresenter(enable) {
        this.#presenter = enable !== undefined
            ? enable
            : !this.#presenter;

        if (!this.#presenter) {
            this.#pointerEnabled = false;
        }
    }

    /**
     * @returns {boolean}
     * @public
     */
    isPointerEnabled() {
        return this.#pointerEnabled;
    }

    /**
     * @public
     */
    sendChannelData(channel, data) {
        this.sendJson({
            ts: new Date().toISOString(),
            type: 'channel',
            id: channel.id,
            subtype: 'data',
            data,
        });
    }

    setFocusedChannel(id) {
        this.#ws.send(
            JSON.stringify({
                type: 'focus',
                id,
            }),
        );
    }

    setPointerPosition(x, y) {
        if (!this.#pointerEnabled) {
            return;
        }

        this.sendJson({
            ts: new Date().toISOString(),
            type: 'pointer',
            x,
            y,
        });
    }

    /**
     * Send data to the websocket server
     * @param {*} data
     * @private
     */
    sendData(data) {
        if (!this.#presenter) {
            return;
        }
        this.#ws.send(data);
    }

    /**
     * Send JSON object to the server
     * @param {*} data
     * @private
     */
    sendJson(data) {
        const str = JSON.stringify(data);
        this.sendData(str);
    }

    /**
     * Subscribe to the event of a specific channel.
     *
     * @param {number} id
     * @param {function} onMessage
     * @param {function} onControl
     * @returns {function} - Function to unsubscribe to the events
     * @public
     */
    subscribeToChannel(id, onMessage, onControl) {
        const obj = {
            onMessage,
            onControl,
        };

        this.#subscriptions[id].push(obj);

        const unsubscribe = () => {
            if (!this.#subscriptions[id]) {
                return;
            }

            const index = this.#subscriptions[id].indexOf(obj);

            this.#subscriptions[id].splice(index, 1);
        };

        return unsubscribe;
    }

    // ============================================================
    // Event Listeners

    /**
     * @private
     */
    fireFocusEvent() {
        // Emit event
        const event = new CustomEvent(
            SocketConnectionEvent.focus,
            {
                detail: {
                    channel: this.getFocusedChannel(),
                },
            },
        );

        this.dispatchEvent(event);
    }

    /**
     * Create a channel
     *
     * @param {number}        id           - ID of the channel to create
     * @param {OperationType} type         - Type of channel
     * @param {Object}        initialState - Initial data of the channel
     * @fires SocketConnection:createChannel
     * @private
     */
    onChannelCreate(id, type, initialState) {
        const focusChannel = this.#channels.length === 0;
        this.#maxChannelId = id;

        this.#subscriptions[id] = [];

        const channel = new Channel(
            id,
            type,
            this,
            initialState,
        );
        this.#channels.push(channel);

        if (focusChannel) {
            this.#focusedChannel = channel;
        }

        // Emit event
        const event = new CustomEvent(
            SocketConnectionEvent.newChannel,
            {
                detail: {
                    channel,
                },
            },
        );

        this.dispatchEvent(event);

        // The first created channel is focused by
        if (focusChannel) {
            this.fireFocusEvent();
        }
    }

    /**
     *
     * @param {Object} message
     * @private
     */
    onChannelMessage(message) {
        const channel = this.getChannel(message.id);
        const subscriptions = this.#subscriptions[message.id];

        // Emit event
        const event = new CustomEvent(
            SocketConnectionEvent.message,
            {
                detail: {
                    channel,
                    message,
                },
            },
        );

        // Notifying all subscribers of the event
        subscriptions.forEach(({ onMessage }) => onMessage(event));
        this.dispatchEvent(event);
    }

    /**
     * @param {Object} message
     * @fires SocketConnection:createChannel
     * @fires SocketConnection:deleteChannel
     * @private
     */
    onControlMessage(message) {
        switch (message.op) {
        case OperationType.create:
            this.onChannelCreate(
                message.id,
                message.chanType,
                message.init,
            );
            break;

        case OperationType.delete:
            this.deleteChannel(
                message.id,
            );
            break;

        default:
            throw new Error(`Unknown operation type: "${message.op}"`);
        }
    }

    /**
     * @param {Object} message
     * @fires SocketConnection:createChannel
     * @fires SocketConnection:deleteChannel
     * @private
     */
    onFocusMessage(message) {
        const channel = this.getChannel(message.id);

        this.#focusedChannel = channel;

        this.fireFocusEvent();
    }

    onGlobalMetadataMessage() {
        return this;
    }

    /**
     * @param {boolean} disabled
     * @param {number}  channel  - Channel currently pointed
     * @param {number}  x
     * @param {number}  y
     * @fires SocketConnection#event:pointerDisabled
     * @fires SocketConnection#event:pointerMove
     */
    onPointerMove({
        x, y, channel, disabled,
    }) {
        this.#pointer = disabled
            ? undefined
            : {
                x,
                y,
                channel: this.getChannel(channel),
            };

        // Emit event
        if (disabled) {
            const event = new CustomEvent(SocketConnectionEvent.pointerDisabled);
            this.dispatchEvent(event);
        }
        else {
            const event = new CustomEvent(
                SocketConnectionEvent.pointerMove,
                {
                    detail: { ...this.#pointer },
                },
            );
            this.dispatchEvent(event);
        }
    }
}

// ============================================================
// JsDoc

/**
 * @event SocketConnection#event:newChannel
 * @see SocketConnectionEvent.newChannel
 * @type {Object}
 * @property {Channel} channel
 * @property {Object}  initialState
 */

/**
 * @event SocketConnection#event:deleteChannel
 * @see SocketConnectionEvent.deleteChannel
 * @type {Object}
 * @property {Channel} channel
 */

/**
 * @event SocketConnection#event:pointerMove
 * @see SocketConnectionEvent.pointerMove
 * @type {Object}
 * @property {Channel} channel - ID of the channel on which is the pointer
 * @property {number}  x
 * @property {number}  y
 */

/**
 * @event SocketConnection#event:pointerDisabled
 * @see SocketConnectionEvent.pointerDisabled
 */

/**
  * @event SocketConnection#event:channelMessage
  * @see SocketConnectionEvent.channelMessage
  * @type {Object}
  * @property {Channel} channel
  * @property {Object}  message
  */
// ============================================================
// Exports
export default SocketConnection;
export {
    SocketConnectionEvent,
};
