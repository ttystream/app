// ============================================================
// Import components

// ============================================================
// Module's constants and variables

/**
 * List of channel event type
 * @enum {string}
 * @readonly
 * @public
 */
const ChannelEvent = {
    /**
     * A new message appears
     * @see ChannelEvent:message
     */
    message: 'message',

    /**
     * A new metadata message appears
     * @see ChannelEvent:metadata
     */
    metadata: 'metadata',

    /**
     * @see ChannelEvent#event:titleChange
     */
    titleChange: 'title-change',
};

// ============================================================
// Component
class Channel extends EventTarget {
    /**
     * @type {Object[]}
     */
    #buffer = [];

    /**
     * Mapping between socket events and channel events
     * @type {WeakMap}
     * @private
     */
    eventMap = new WeakMap();

    /**
     * Initial state of the channel
     * @type {Object}
     */
    #initialState;

    /**
     * @type {SocketConnection}
     */
    #source;

    /**
     * Title of the channel
     * @type {string}
     */
    #title;

    /**
     *
     * @param {number}           id
     * @param {string}           type
     * @param {SocketConnection} source
     */
    constructor(id, type, source, initialState) {
        super();
        this.id = id;
        this.type = type;
        this.#source = source;
        this.#title = `type: ${type}`;
        this.#initialState = initialState;

        this.initialize();
    }

    /**
     * @return {Object[]}
     * @public
     */
    getBuffer() {
        return this.#buffer.slice(0);
    }

    /**
     * Return the initial state of the channel
     * @public
     */
    getInitialState() {
        return this.#initialState;
    }

    getTitle() {
        return this.#title;
    }

    /**
     * Initialize the channel
     * @private
     */
    initialize() {
        this.#source.subscribeToChannel(
            this.id,
            this.onMessage.bind(this),
        );
    }

    sendData(data) {
        this.#source.sendChannelData(
            this,
            data,
        );
    }

    // ============================================================
    // Event listeners

    /**
     * @fires Channel:Message
     * @param {Event} event
     * @private
     */
    onDataMessage(event) {
        const { message } = event.detail;

        let channelEvent = this.eventMap.get(event);

        // Building the channel event or using the cached one
        if (!channelEvent) {
            const customEvent = new CustomEvent(
                ChannelEvent.message,
                {
                    detail: {
                        channel: this,
                        data: message.data,
                        state: undefined,
                        messageId: message.messageId,
                        timestamp: message.timestamp,
                    },
                },
            );

            customEvent.detail.setState = ((chanEvent, state) => {
                // eslint-disable-next-line no-param-reassign
                chanEvent.state = state;
            }).bind(undefined, customEvent);

            this.eventMap.set(event, customEvent);
            channelEvent = customEvent;
        }

        this.#buffer.push(channelEvent);

        // Notifying all subscribers of the event
        this.dispatchEvent(channelEvent);
    }

    /**
     *
     * @param {CustomEvent} event
     * @private
     */
    onMessage(event) {
        const { message } = event.detail;
        switch (message.subtype) {
        case 'data':
            this.onDataMessage(event);
            break;
        case 'metadata':
            this.onMetadataMessage(event);
            break;

        default:
            throw new Error(`Unknown event subtype: ${event.detail.subtype}`);
        }
    }

    /**
     * @fires Channel#event:metadata
     * @fires Channel#event:titleChange
     * @param {CustomEvent} event
     * @private
     */
    onMetadataMessage(event) {
        const { data } = event.detail;

        this.fireMetadataEvent(data);

        // Event: title change
        if (data.title !== this.#title) {
            const oldTitle = this.#title;
            this.#title = data.title;

            this.fireTitleChangeEvent(oldTitle, this.#title);
        }
    }

    // ============================================================
    // Fireing event methods

    /**
     * @fires Channel#event:metadata
     * @param {Object} data
     * @private
     */
    fireMetadataEvent(data) {
        // Emit event
        const customEvent = new CustomEvent(
            ChannelEvent.metadata,
            {
                detail: {
                    data,
                    channel: this,
                },
            },
        );

        // Notifying all subscribers of the event
        this.dispatchEvent(customEvent);
    }

    /**
     * @fires Channel#event:titleChange
     * @param {string} oldTitle
     * @param {string} newTitle
     * @private
     */
    fireTitleChangeEvent(oldTitle, newTitle) {
        // Emit event
        const customEvent = new CustomEvent(
            ChannelEvent.metadata,
            {
                detail: {
                    channel: this,
                    oldTitle,
                    newTitle,
                },
            },
        );

        // Notifying all subscribers of the event
        this.dispatchEvent(customEvent);
    }
}

// ============================================================
// JsDoc

/**
 * Channel message event.
 *
 * @event Channel:Message
 * @see ChannelEvent.message
 * @type {Object}
 * @property {Object}     detail
 * @property {Channel}    detail.channel
 * @property {Object}     detail.data
 * @property {function:*} detail.setState
 * @property {*}          detail.state
 * @property {Date}       detail.timestamp
 */

/**
 * @event Channel:MetadataUpdate
 * @see ChannelEvent.metadata
 * @type {Object}
 * @property {Object}  detail
 * @property {Channel} detail.channel
 * @property {Object}  detail.data
 * @property {Date}    detail.timestamp
 */

/**
 * @event Channel#event:titleChange
 * @see ChannelEvent.titleChange
 * @type {Object}
 * @property {Object}  detail
 * @property {Channel} detail.channel
 * @property {Object}  detail.oldTitle
 * @property {Object}  detail.newTitlte
 * @property {Date}    detail.timestamp
 */

// ============================================================
// Exports

export default Channel;
export { ChannelEvent };
