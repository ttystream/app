// ============================================================
// Import packages
import React from 'react';

// ============================================================
// Component
const SocketConnectionContext = React.createContext();

export {
    SocketConnectionContext,
};
