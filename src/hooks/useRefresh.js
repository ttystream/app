// ============================================================
// Import packages
import { useRef, useState } from 'react';

import useStable from './useStable';

// ============================================================
// Hook

/**
 * Return a function that when it's called, force the refresh of the states.
 * The returned function identity is guaranted stable and won't change on re-renders.
 * It is safe to ommit from useEffect or useCallback dependency list.
 * @return {function}
 */
function useRefresh() {
    const setId = useState(0)[1];
    const ref = useRef(0);

    return useStable(() => {
        ref.current += 1;
        setId(ref.current);
    });
}

// ============================================================
// Exports
export default useRefresh;
