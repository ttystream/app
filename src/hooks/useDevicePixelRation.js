// ============================================================
// Import packages
import {
    useEffect,
    useState,
} from 'react';

// ============================================================
// Module's constants and variables

const listeners = [];
let previousDevicePixelRatio;

window.addEventListener('resize', () => {
    if (window.devicePixelRatio === previousDevicePixelRatio) {
        return;
    }

    previousDevicePixelRatio = window.devicePixelRatio;

    listeners.forEach((listener) => listener(window.devicePixelRatio));
});
// ============================================================
// Hook
function useDevicePixelRatio() {
    const [devicePixelRatio, setDevicePixelRatio] = useState(window.devicePixelRatio);

    useEffect(() => {
        listeners.push(setDevicePixelRatio);

        return () => {
            const indexOf = listeners.indexOf(setDevicePixelRatio);
            listeners.splice(indexOf, 1);
        };
    }, []);

    return devicePixelRatio;
}

// ============================================================
// Exports
export default useDevicePixelRatio;
