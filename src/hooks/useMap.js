// ============================================================
// Import modules
import useRefresh from './useRefresh';
import useStable from './useStable';

// ============================================================
// Hook
function useMap(iterable) {
    const refresh = useRefresh();

    return useStable(
        new HookedMap(
            iterable,
            refresh,
        ),
    );
}

// ============================================================
// Class
class HookedMap {
    #map;

    #refresh;

    constructor(iterable, refresh) {
        this.#map = new Map(iterable);
        this.#refresh = refresh;
    }

    [Symbol.iterator]() {
        return this.#map[Symbol.iterator]();
    }

    #prepare = () => {
        this.#map = new Map(this.#map);
    };

    clear() {
        this.#map = new Map();
        this.#refresh();
    }

    delete(key) {
        this.#prepare();
        return this.#map.delete(key);
    }

    entries() {
        return this.#map.entries();
    }

    forEach(...args) {
        this.#map.forEach(...args);
    }

    get(key) {
        return this.#map.get(key);
    }

    has(key) {
        return this.#map.has(key);
    }

    keys() {
        return this.#map.keys();
    }

    set(key, value) {
        this.#prepare();
        this.#map.set(key, value);

        return this;
    }

    values() {
        return this.#map.values();
    }
}

// ============================================================
// Exports
export default useMap;
