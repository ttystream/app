// ============================================================
// Import modules
import useRefresh from './useRefresh';
import useStable from './useStable';

// ============================================================
// Hook
function useArray(initial = []) {
    const refresh = useRefresh();

    return useStable(
        new HookedArray(
            initial,
            refresh,
        ),
    );
}

// ============================================================
// Class
class HookedArray {
    #array;

    #refresh;

    [Symbol.iterator]() {
        return this.#array[Symbol.iterator]();
    }

    constructor(array = [], refresh) {
        this.#array = [...array];
        this.#refresh = refresh;
    }

    #call = (name, callback, thisArg) => {
        if (typeof callback !== 'function') {
            throw new TypeError(`${callback} is not a function`);
        }

        return this.#array[name](
            (element, index) => callback.call(thisArg, element, index, this),
        );
    };

    #prepare = () => {
        this.#array = [...this.#array];
    }

    concat(...args) {
        const array = this.#array.concat(...args);
        return new HookedArray(array, this.#refresh);
    }

    copyWithin(...args) {
        this.#prepare();
        this.#array.copyWithin(...args);
        this.#refresh();
        return this;
    }

    empty() {
        this.#refresh();
        this.#array = [];
        this.#refresh();
    }

    entries(...args) {
        return this.#array.entries(...args);
    }

    every(callback, thisArg) {
        return this.#call('every', callback, thisArg);
    }

    fill(...args) {
        this.#prepare();

        this.#array.fill(...args);
        this.#refresh();
        return this;
    }

    filter(callback, thisArg) {
        this.#prepare();
        const array = this.#call('filter', callback, thisArg);
        this.#refresh();
        return new HookedArray(array, this.#refresh);
    }

    find(callback, thisArg) {
        return this.#call('find', callback, thisArg);
    }

    findIndex(callback, thisArg) {
        return this.#call('findIndex', callback, thisArg);
    }

    flat(...args) {
        const array = this.#array.flat(...args);
        return new HookedArray(array, this.#refresh);
    }

    flatMap(callback, thisArg) {
        const array = this.#call('flatMap', callback, thisArg);
        return new HookedArray(array, this.#refresh);
    }

    forEach(callback, thisArg) {
        this.#call('forEach', callback, thisArg);
    }

    get(index) {
        return this.#array[index];
    }

    includes(...args) {
        return this.#array.includes(...args);
    }

    indexOf(...args) {
        return this.#array.indexOf(...args);
    }

    join(...args) {
        return this.#array.join(...args);
    }

    keys() {
        return this.#array.keys();
    }

    lastIndexOf(...args) {
        return this.#array.lastIndexOf(...args);
    }

    map(callback, thisArg) {
        const array = this.#call('map', callback, thisArg);
        return new HookedArray(array, this.#refresh);
    }

    pop(...args) {
        this.#prepare();
        const result = this.#array.pop(...args);
        this.#refresh();
        return result;
    }

    push(...args) {
        this.#array = [...this.#array];
        const arr = this.#array.push(...args);
        this.#refresh();
        return arr;
    }

    reduce(callback, initialValue) {
        return this.#array.reduce(
            (accumulator, currentvalue, index) => callback.call(undefined, accumulator, currentvalue, index, this),
            initialValue,
        );
    }

    reduceRight(callback, initialValue) {
        return this.#array.reduceRight(
            (accumulator, currentvalue, index) => callback.call(undefined, accumulator, currentvalue, index, this),
            initialValue,
        );
    }

    reverse() {
        this.#prepare();
        this.#array.reverse();
        this.#refresh();
        return this;
    }

    set(index, value) {
        this.#prepare();
        this.#array[index] = value;
        this.#refresh();
    }

    shift(...args) {
        this.#prepare();
        const result = this.#array.shift(...args);
        this.#refresh();
        return result;
    }

    slice(...args) {
        const array = this.#array.slice(...args);
        return new HookedArray(array, this.#refresh);
    }

    some(callback, thisArg) {
        return this.#call('some', callback, thisArg);
    }

    sort(compareFunction) {
        this.#prepare();
        this.#array.sort(compareFunction);
        this.#refresh();
        return this;
    }

    splice(...args) {
        this.#prepare();
        const deletedElements = this.#array.splice(...args);
        this.#refresh();
        return new HookedArray(deletedElements, this.#refresh);
    }

    toLocaleString(...args) {
        return this.#array.toLocaleString(...args);
    }

    toString() {
        return this.#array.toString();
    }

    unshift(...args) {
        this.#prepare();
        const result = this.#array.unshift(...args);
        this.#refresh();
        return result;
    }

    values() {
        return this.#array.values();
    }
}

// ============================================================
// Exports
export default useArray;
