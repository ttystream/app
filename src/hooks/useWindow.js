// ============================================================
// Import packages
import {
    useEffect,
    useState,
} from 'react';

// ============================================================
// Module's constants and variables

const listeners = {
    devicePixelRatio: [],
    innerHeight: [],
    innerWidth: [],
    screen: {
        availHeight: [],
        availWidth: [],
    },
};

const previousValues = {
    devicePixelRatio: undefined,
    innerHeight: window.innerHeight,
    innerWidth: window.innerWidth,
    screen: {
        availHeight: undefined,
        availWidth: undefined,
    },
};

window.addEventListener('resize', () => {
    dispatch('devicePixelRatio');
    dispatch('innerWidth');
    dispatchScreen('availHeight');
    dispatchScreen('availWidth');
});

function dispatch(name) {
    // Device pixel ratio
    if (window[name] !== previousValues[name]) {
        previousValues[name] = window[name];
        listeners[name].forEach((listener) => listener(window[name]));
    }
}

function dispatchScreen(name) {
    // Device pixel ratio
    if (window.screen[name] !== previousValues.screen[name]) {
        previousValues.screen[name] = window.screen[name];
        listeners.screen[name].forEach((listener) => listener(window.screen[name]));
    }
}

// ============================================================
// Hook
function useWindow() {
    return {
        devicePixelRatio: useDevicePixelRatio(),
        innerWidth: useInnerWidth(),
        innerHeight: useInnerHeight(),
        screen: {
            availHeight: useScreenAvailHeight(),
        },
    };
}

function useProperty(name) {
    const [value, setValue] = useState(previousValues[name]);

    useEffect(() => {
        listeners[name].push(setValue);

        return () => {
            const indexOf = listeners[name].indexOf(setValue);
            listeners[name].splice(indexOf, 1);
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return value;
}

function useDevicePixelRatio() {
    return useProperty('devicePixelRatio');
}

function useInnerWidth() {
    return useProperty('innerWidth');
}

function useInnerHeight() {
    return useProperty('innerHeight');
}

function useScreenAvailHeight() {
    const [availHeight, setAvailHeight] = useState(previousValues.screen.availHeight);

    useEffect(() => {
        listeners.screen.availHeight.push(setAvailHeight);

        return () => {
            const indexOf = listeners.screen.availHeight.indexOf(setAvailHeight);
            listeners.screen.availHeight.splice(indexOf, 1);
        };
    }, []);

    return availHeight;
}

// ============================================================
// Exports
export default useWindow;

export {
    useDevicePixelRatio,
    useInnerHeight,
    useInnerWidth,
    useScreenAvailHeight,
};
