// ============================================================
// Import packages
import { useEffect } from 'react';

// ============================================================
// Import modules
import useRefresh from './useRefresh';

// ============================================================
// Component

/**
 * Hook that will trigger a refresh each time one of the provided event occurs.
 * @param {EventTarget} eventTarget
 * @param {string[]}    events
 * @public
 */
function useEventTarget(eventTarget, events = []) {
    const refresh = useRefresh();

    useEffect(() => {
        events.forEach((event) => {
            eventTarget.addEventListener(event, refresh);
        });

        return () => events.forEach((event) => {
            eventTarget.removeEventListener(event, refresh);
        });
    }, [eventTarget, events, refresh]);
}

// ============================================================
// Exports
export default useEventTarget;
