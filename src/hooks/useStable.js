// ============================================================
// Import packages
import { useState } from 'react';

// ============================================================
// Hook
function useStable(value) {
    return useState([value])[0][0];
}

// ============================================================
// Exports
export default useStable;
