// ============================================================
// Import packages
import { useRef, useState } from 'react';

// ============================================================
// Import modules
import useRefresh from './useRefresh';

// ============================================================
// Hooks

function useObject(initial = {}) {
    const ref = useRef(initial);
    const refresh = useRefresh();

    const [setter] = useState((key, value) => {
        ref.current = {
            ...ref.current,
            [key]: value,
        };
        refresh();
    });

    return [
        { ...ref.current },
        setter,
    ];
}

// ============================================================
// Exports
export default useObject;
