// ============================================================
// Module's constants and variables
const dimensions = {};

const element = document.createElement('span');
element.innerText = 'W';
element.classList.add('char-size-measurer');

// ============================================================
// Hook
function getCharDimensions(
    fontFamily,
    fontSize,
) {
    if (!dimensions[fontFamily]) {
        dimensions[fontFamily] = {};
    }

    const fontSizes = dimensions[fontFamily];

    if (fontSizes[fontSize] === undefined) {
        element.style.fontFamily = fontFamily;
        element.style.fontSize = `${fontSize}px`;

        document.body.appendChild(element);
        const { height, width } = element.getBoundingClientRect();
        fontSizes[fontSize] = { height, width };
        element.parentElement.removeChild(element);
    }

    return { ...fontSizes[fontSize] };
}

//  // ============================================================
// Exports
export default getCharDimensions;
