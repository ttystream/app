// ============================================================
// Import packages
import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

// ============================================================
// Component
function FontBar({
    style,
    setOption,
}) {
    const { fontSize } = style;

    return (
        <Container>
            <IconButton
                aria-label="delete"
                type="button"
                disabled={fontSize <= 2}
                onClick={() => setOption('fontSize', fontSize - 1)}
            >
                <RemoveIcon />
            </IconButton>
            {fontSize}
            <IconButton
                aria-label="delete"
                type="button"
                onClick={() => setOption('fontSize', fontSize + 1)}
            >
                <AddIcon />
            </IconButton>
        </Container>
    );
}

FontBar.propTypes = {
    style: PropTypes.shape({
        fontSize: PropTypes.number.isRequired,
        fontFamily: PropTypes.string.isRequired,
    }).isRequired,
    setOption: PropTypes.func.isRequired,
};

// ============================================================
// Exports
export default FontBar;
