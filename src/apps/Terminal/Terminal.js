// ============================================================
// Import packages
import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react';
import PropTypes from 'prop-types';
import { Terminal as XTerm } from 'xterm';
import CardActions from '@material-ui/core/CardActions';

import { useDevicePixelRatio } from '../../hooks';
import FontBar from './FontBar';
import Channel, { ChannelEvent } from '../../Channel';
import { getCharDimensions } from '../../helpers';

// ============================================================
// Component
function Terminal({
    channel,
}) {
    const {
        loading,
        divTerm,
        setOption,
        style,
    } = useTerminal(channel);

    const fontBar = !loading
        ? <FontBar style={style} setOption={setOption} />
        : undefined;

    const pointer = <div ref={divTerm} style={{ width: `${style.width}px` }} />;


    return (
        <div className="terminal">
            <CardActions disableSpacing>
                {fontBar}
            </CardActions>
            {pointer}
        </div>
    );
}

Terminal.propTypes = {
    channel: PropTypes.instanceOf(Channel).isRequired,
};

// ============================================================
// Helpers

/**
 * Creating the terminal
 * The created terminal will be persisted during the whole life of the component.
 * @param {Channel} channel
 * @param {number}  maxWidth
 * @returns {number} - Current displayed page
 */
function useTerminal(channel) {
    const initialState = useMemo(() => channel.getInitialState(), [channel]);

    const divTerm = useRef();
    const [terminal, setTerminal] = useState();
    const [loading, setLoading] = useState(true);

    const {
        setOption,
        loading: styleLoading,
        style,
    } = useStyle(
        terminal,
    );

    useEffect(() => {
        const term = new XTerm({
            cols: initialState.cols,
            rows: initialState.rows,
            disableStdin: true,
        });

        setTerminal(term);

        const onMessageBind = onMessage.bind(undefined, term);

        term.open(divTerm.current);

        channel.addEventListener(ChannelEvent.message, onMessageBind);
        channel.getBuffer().forEach(onMessageBind);

        setLoading(false);
        // On unmount, we remove all listeners
        return () => {
            channel.removeEventListener(ChannelEvent.message, onMessageBind);
        };
    }, [channel, initialState.cols, initialState.rows]);

    return {
        style,
        divTerm,
        loading: loading || styleLoading,
        setOption,
    };
}

function useStyle(
    terminal,
    maxWidth,
) {
    const devicePixelRatio = useDevicePixelRatio();

    // Terminal parameters
    const [cols, setCols] = useState();
    const [rows, setRows] = useState();
    const [initialized, setInitialized] = useState(false);
    const [loading, setLoadnig] = useState(true);

    // CSS Style
    const [fontFamily, setFontFamily] = useState();
    const [fontSize, setFontSize] = useState();
    const [letterSpacing, setLetterSpacing] = useState();
    const [width, setWidth] = useState();

    if (!initialized && terminal) {
        setCols(terminal.getOption('cols'));
        setRows(terminal.getOption('rows'));

        setFontFamily(terminal.getOption('fontFamily'));
        setFontSize(terminal.getOption('fontSize'));
        setLetterSpacing(terminal.getOption('letterSpacing'));

        setInitialized(true);
        setLoadnig(false);
    }

    const setOption = useCallback((key, value) => {
        terminal.setOption(key, value);
        switch (key) {
        case 'fontFamily':
            setFontFamily(value);
            break;

        case 'fontSize':
            setFontSize(value);
            break;

        case 'letterSpacing':
            setLetterSpacing(value);
            break;
        default:
            break;
        }
    }, [terminal]);

    useMemo(
        () => {
            if (!terminal || !initialized) {
                return;
            }

            // If a maxWidth is defined, we adpat the font size
            if (maxWidth) {
                const [calculatedWidth, foundFontSize] = calcFontSizeFromWidth(
                    maxWidth,
                    fontFamily,
                    letterSpacing,
                    cols,
                    fontSize,
                    devicePixelRatio,
                );

                setWidth(calculatedWidth);
                setOption('fontSize', foundFontSize);
            }
            else {
                const divWidth = calcWidthFromFont(
                    fontSize,
                    fontFamily,
                    letterSpacing,
                    cols,
                    devicePixelRatio,
                );

                setWidth(divWidth);
            }
        },
        [
            cols,
            devicePixelRatio,
            fontFamily,
            fontSize,
            initialized,
            letterSpacing,
            maxWidth,
            setOption,
            terminal,
        ],
    );

    return {
        loading,
        setOption,
        style: {
            cols,
            fontSize,
            fontFamily,
            letterSpacing,
            rows,
            width,
        },
    };
}

/**
 * Calculate the width of the terminal.
 * This calculation is based on the xterm calcualtion that can be found here:
 * https://github.com/xtermjs/xterm.js/blob/master/src/browser/renderer/dom/DomRenderer.ts#L105
 * @param {number}         cols
 * @param {string}         fontFamily
 * @param {number}         fontSize
 * @param {number}         letterSpacing
 */
function calcWidthFromFont(fontSize, fontFamily, letterSpacing, cols, devicePixelRatio) {
    return calcWidth(
        getCharDimensions(fontFamily, fontSize).width,
        letterSpacing,
        cols,
        devicePixelRatio,
    );
}

function calcFontSizeFromWidth(
    targetWidth,
    fontFamily,
    letterSpacing,
    cols,
    actualFontSize,
    devicePixelRatio,
) {
    let currentFontSize = actualFontSize;

    let { width: charWidth } = getCharDimensions(fontFamily, currentFontSize);
    let previousWidth = calcWidth(charWidth, letterSpacing, cols, devicePixelRatio);
    const wasTooBig = previousWidth > targetWidth;

    // eslint-disable-next-line no-constant-condition
    while (true) {
        currentFontSize += wasTooBig ? -1 : 1;
        charWidth = getCharDimensions(fontFamily, currentFontSize).width;

        const width = calcWidth(charWidth, letterSpacing, cols, devicePixelRatio);

        if (wasTooBig && width <= targetWidth) {
            return [width, currentFontSize];
        }

        if (!wasTooBig && width > targetWidth) {
            return [
                previousWidth,
                currentFontSize - 1,
            ];
        }

        previousWidth = width;
    }
}

function calcWidth(charWidth, letterSpacing, cols, devicePixelRatio) {
    const scaledCharWidth = Math.floor(charWidth * devicePixelRatio);
    const scaledCellWidth = scaledCharWidth + Math.round(letterSpacing);
    const scaledWidth = scaledCellWidth * cols;
    const divWidth = Math.round(scaledWidth / devicePixelRatio);

    return divWidth;
}

/**
 * Listen to ChannelStream and update the terminal accordingly.
 * @param {XTerm}           term
 * @param {Channel:Message} data - Received data
 */
function onMessage(term, event) {
    const { data } = event.detail;

    switch (data.type) {
    case 'action': {
        term.write(data.content);
        break;
    }

    case 'resize': {
        term.resize(data.cols, data.rows);
        break;
    }

    default:
        throw new Error(`Unknown message type: ${data.type}`);
    }
}

// ============================================================
// Exports
export default Terminal;
