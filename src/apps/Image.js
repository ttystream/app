// ============================================================
// Import packages
import React from 'react';
import PropTypes from 'prop-types';

import Channel from '../Channel';
import useChannel from '../useChannel';

// ============================================================
// Component
function Image({
    initialState,
    channel,
}) {
    const { title } = useChannel(channel);
    const { file } = initialState;

    return (
        <img
            src={file}
            alt={title}
        />
    );
}

Image.propTypes = {
    channel: PropTypes.instanceOf(Channel).isRequired,
    initialState: PropTypes.shape({
        file: PropTypes.string.isRequired,
    }).isRequired,
};

// ============================================================
// Exports
export { Image as App };
