// ============================================================
// Import packages
import React, { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import Form from 'react-bootstrap/Form';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';

// ============================================================
// Component

function CreateForm({
    onCreate,
    onCancel,
}) {
    const [isValidUrl, setValidUrl] = useState(false);
    const inputRef = useRef();

    const onSubmit = useCallback((event) => {
        const form = event.currentTarget;
        if (form.checkValidity()) {
            const file = inputRef.current.value;
            onCreate({
                file,
                initial_page: 1,
            });
        }

        event.preventDefault();
        event.stopPropagation();
    }, [onCreate]);

    const onChange = useCallback((event) => {
        setValidUrl(event.target.value.length && event.target.validity.valid);
    }, []);

    return (
        <Form onSubmit={onSubmit} className="col-sm-12">
            <Form.Group>
                <h1>Présenter un PDF</h1>
                <Form.Control
                    type="url"
                    placeholder="Url du document (ex: https://example.com/cours.pdf)"
                    onChange={onChange}
                    minLength={10}
                    ref={inputRef}
                />
            </Form.Group>
            <ButtonToolbar>
                <Button variant="secondary" onClick={onCancel} className="mr-2">Annuler</Button>
                <Button variant="primary" type="submit" disabled={!isValidUrl}>Creation</Button>
            </ButtonToolbar>
        </Form>
    );
}

CreateForm.propTypes = {
    onCreate: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

// ============================================================
// Exports
export default CreateForm;
