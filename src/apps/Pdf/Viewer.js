// ============================================================
// Import packages
import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Document, Page } from 'react-pdf';
import InfiniteScroll from 'react-infinite-scroll-component';

import Channel, { ChannelEvent } from '../Channel';

// ============================================================
// Component
function Viewer({
    channel,
    initialState,
}) {
    return (
        <InfiniteScroll>
        </InfiniteScroll>
    );
}

Viewer.propTypes = {
    initialState: PropTypes.shape({
        file: PropTypes.string.isRequired,
    }).isRequired,
};

// ============================================================
// Exports
export default Viewer;
