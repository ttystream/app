// ============================================================
// Import packages
import React from 'react';
import PropTypes from 'prop-types';
import { Page } from 'react-pdf';

import withPointer from '../../withPointer';

// ============================================================
// Component
function CurrentPage({
    pageNumber,
    availHeight,
}) {
    return pageNumber !== undefined
        ? (
            <Page
                pageNumber={pageNumber}
                height={availHeight}
                orientation="landscape"
            />
        )
        : <> </>;
}

CurrentPage.defaultProps = {
    pageNumber: undefined,
    availHeight: undefined,
};

CurrentPage.propTypes = {
    pageNumber: PropTypes.number,
    availHeight: PropTypes.number,
};

// ============================================================
// Exports
export default withPointer(CurrentPage);
