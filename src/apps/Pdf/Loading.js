// ============================================================
// Import packages
import React from 'react';

// ============================================================
// Component
function Loading() {
    return (
        <div>Loading...</div>
    );
}

// ============================================================
// Exports
export default Loading;
