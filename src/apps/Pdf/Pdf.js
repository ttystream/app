// ============================================================
// Import packages
import React, {
    useCallback, useEffect, useMemo, useState,
} from 'react';
import PropTypes from 'prop-types';
import { Document, Page } from 'react-pdf';

import Loading from './Loading';
import Channel, { ChannelEvent } from '../../Channel';

import CurrentPage from './CurrentPage';

// ============================================================
// Component
function Pdf({
    availHeight,
    channel,
    setTitle,
}) {
    const {
        file,
        currentPage,
        onLoadSuccess,
        pages,
    } = usePdf(channel, setTitle);

    let content;

    const onPageSelect = useCallback((event) => {
        const pageNumber = Number(event.currentTarget.value);
        channel.sendData({
            type: 'page',
            page: pageNumber,
        });
    }, [channel]);

    if (currentPage) {
        const listPages = pages.map((page) => {
            const displayClass = currentPage.number === page.number ? 'pdf-page-displayed' : '';
            return (
                <button
                    className={`pdf-listPages-item ${displayClass}`}
                    key={page.number}
                    type="button"
                    onClick={onPageSelect}
                    value={page.number}
                >
                    <Page
                        pageNumber={page.number}
                        width={200}
                        orientation="landscape"
                    />
                    <span className="pdf-page-number">{page.number}</span>
                </button>
            );
        });

        content = (
            <>
                <div className="pdf-listPages">
                    {listPages}
                </div>
                <div className="pdf-page">
                    <CurrentPage pageNumber={currentPage.number} availHeight={availHeight} />
                </div>
            </>
        );
    }
    else {
        content = <Loading />;
    }

    return (
        <div className="app-pdf">
            <div>
                <Document file={file} onLoadSuccess={onLoadSuccess} className="pdf-renderer">
                    <div className="pdf-content" style={{ height: availHeight }}>
                        {content}
                    </div>
                </Document>
            </div>
        </div>
    );
}

Pdf.defaultProps = {
    availHeight: undefined,
};

Pdf.propTypes = {
    availHeight: PropTypes.number,
    channel: PropTypes.instanceOf(Channel).isRequired,
    setTitle: PropTypes.func.isRequired,
};


// ============================================================
// Helpers

/**
 * @typedef {Object} UsePdfReturn
 * @property {function} onLoadSuccess - Onload success callback expected by react-pdf/Document
 * @property {string}   UsePdfReturn
 * @property {boolean}  loading       - Indicate if the PDF is still loading or not
 * @property {number}   nbPages       - Number of pages of the PDF. Will be empty until the PDF has finish loading
 * @property {number}   currentPage   - The currently displayed page
 * @property {Object[]} pages         - List of all pages.
 *                                      Each iteam match a page. The page number can be deduce from the index, since
 *                                      pageNumber = index + 1.
 * @property {number}   pages.height  - Height of the page
 * @property {number}   pages.width   - Width of the page
 * @property {number}   pages.number  - Page number
 */

/**
 * The hook behind the PDF component.
 * @param {Channel} channel
 * @param {number}  initialPage - Initial PDF page displayed
 * @returns {UsePdfReturn}
 */
function usePdf(channel, setTitle) {
    const {
        initial_page: initialPage,
        file,
    } = useMemo(
        () => channel.getInitialState(),
        [channel],
    );

    const [currentPage, setCurrentPage] = useState(initialPage);
    const [loadingData, onLoadSuccess] = useState();
    const nbPages = useMemo(() => loadingData && loadingData.numPages, [loadingData]);

    const [pages, setPageData] = useState();

    // Loading PDF file
    useEffect(() => {
        const loadPageData = async () => {
            if (!loadingData) {
                return;
            }

            const metadataPromise = loadingData.getMetadata();

            // Fetching PDF pages metadata
            const promises = [...Array(nbPages)].map(async (empty, index) => {
                const data = await loadingData.getPage(index + 1);
                await data.annotationsPromise;
                return {
                    height: data.view[3],
                    width: data.view[2],
                    number: data.pageIndex + 1,
                };
            });

            const [metadata, listPages] = await Promise.all([
                metadataPromise,
                Promise.all(promises),
            ]);

            setTitle(metadata.info.Title);
            setPageData(listPages);
        };

        loadPageData();
    }, [loadingData, nbPages, setTitle]);

    // Subscribing to channel
    useEffect(() => {
        const onMessageBind = onMessage.bind(undefined, setCurrentPage);
        channel.addEventListener(ChannelEvent.message, onMessageBind);

        return () => {
            channel.removeEventListener(ChannelEvent.message, onMessageBind);
        };
    }, [channel]);

    return {
        currentPage: pages ? pages[currentPage - 1] : undefined,
        file,
        loading: !loadingData,
        nbPages,
        onLoadSuccess,
        pages,
        setCurrentPage,
    };
}

function onMessage(setCurrentPage, { detail: { data } }) {
    if (data.type === 'page') {
        setCurrentPage(data.page);
    }
    else {
        throw new Error(`Unknown type: "${data.type}"`);
    }
}

// ============================================================
// Exports
export default Pdf;
