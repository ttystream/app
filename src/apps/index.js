import * as Image from './Image';
import * as Pdf from './Pdf';
import * as Terminal from './Terminal';

const types = {
    image: 'image',
    pdf: 'pdf',
    terminal: 'tty',
};

/**
 * Map the type of channels with the view component
 */
const map = {
    [types.image]: Image,
    [types.pdf]: Pdf,
    [types.terminal]: Terminal,
};

export {
    map,
    types,
    Image,
    Pdf,
    Terminal,
};
