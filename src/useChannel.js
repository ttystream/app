// ============================================================
// Hook
import {
    useEffect,
    useState,
} from 'react';

/**
 *
 * @param {Channel} channel
 */
function useChannel(channel, initialize) {
    const [title, setTitle] = useState(channel.title);

    useEffect(() => {
        channel.addEventListener('metadata', ({ data }) => {
            if (data.title !== undefined) {
                setTitle(data.title);
            }
        });

        initialize(channel);
    }, [channel, initialize]);

    return {
        stream: channel.stream,
        title,
    };
}

// ============================================================
// Exports
export default useChannel;
