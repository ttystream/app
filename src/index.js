// ============================================================
// Import packages
import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';


// ============================================================
// Import modules
import {
    App,
} from './App';

// ============================================================
// Functions
/**
 * Initialize the application
 */
function initialize() {
    const root = document.createElement('div');
    root.setAttribute('id', 'root');

    document.querySelector('body').appendChild(root);

    const wsurl = getWsUrl('/channels');

    ReactDOM.render(
        <>
            <CssBaseline />
            <App wsurl={wsurl} />
        </>,
        root,
    );
}

function getWsUrl(path) {
    const loc = window.location;
    let newUri = loc.protocol === 'https:'
        ? 'wss:'
        : 'ws:';

    newUri += `//${loc.host}${path}`;

    return newUri;
}

document.addEventListener('DOMContentLoaded', initialize);
