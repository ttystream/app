// ============================================================
// Import packages
import React, {
    useContext,
    useEffect,
    useRef,
    useState,
}
    from 'react';

// ============================================================
// Import modules
import { SocketConnectionEvent } from './SocketConnection';
import { SocketConnectionContext } from './contexts';

// ============================================================
// Component

function withPointer(Component) {
    return function Pointer(props) {
        const {
            divRef,
            isPresenter,
            x,
            y,
        } = usePointer();

        const pointerClass = isPresenter ? 'pointer-presenter' : '';

        return (
            <div
                className="pointerWrapper"
                ref={divRef}
            >
                <div
                    className={`pointer ${pointerClass}`}
                    style={{ top: `${y}%`, left: `${x}%` }}
                />
                { /* eslint-disable-next-line react/jsx-props-no-spreading */ }
                <Component {...props} />
            </div>
        );
    };
}

function usePointer() {
    const [{ x, y }, setPosition] = useState({});
    const [isPresenter, setPresenter] = useState(false);
    const divRef = useRef();

    const socketConnection = useContext(SocketConnectionContext);

    useEffect(() => {
        const divNode = divRef.current;

        if (!socketConnection || !divNode) {
            return undefined;
        }

        setPresenter(socketConnection.isPresenter());

        // If presenter, we detect the pointer move and forward it to the socket connection
        if (isPresenter) {
            const onPresenterMoveListener = onPresenterMove.bind(undefined, socketConnection);
            divNode.addEventListener('mousemove', onPresenterMoveListener);

            return () => {
                divNode.removeEventListener('mousemove', onPresenterMoveListener);
            };
        }

        // If not a presenter, we update the pointer position
        const onMoveListener = onMove.bind(undefined, setPosition);
        const onDisabledListener = onDisabled.bind(undefined, setPosition);

        socketConnection.addEventListener(SocketConnectionEvent.pointerMove, onMoveListener);
        socketConnection.addEventListener(SocketConnectionEvent.pointerDisabled, onDisabledListener);

        return () => {
            socketConnection.removeEventListener(SocketConnectionEvent.pointerMove, onMoveListener);
            socketConnection.removeEventListener(SocketConnectionEvent.pointerDisabled, onDisabledListener);
        };
    }, [socketConnection, isPresenter]);

    return {
        divRef,
        isPresenter,
        x,
        y,
    };
}

function onPresenterMove(socketConnection, event) {
    const relativeX = event.pageX - event.currentTarget.offsetLeft;
    const relativeY = event.pageY - event.currentTarget.offsetTop;

    const x = (relativeX / event.currentTarget.offsetWidth) * 100.0;
    const y = (relativeY / event.currentTarget.offsetHeight) * 100.0;

    socketConnection.setPointerPosition(x, y);
}

function onMove(setPosition, { detail: { x, y } }) {
    setPosition({ x, y });
}

function onDisabled(setPosition) {
    setPosition({});
}

// ============================================================
// Exports
export default withPointer;
