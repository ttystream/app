// ============================================================
// Import packages
import { useMemo } from 'react';

// ============================================================
// Import modules
import {
    useScreenAvailHeight,
} from '../hooks';

// ============================================================
// Hook

/**
 *
 * @param {Array.<{channel: Channel, scene: number}>} channels
 * @returns {{cols: number, rowHeight: number, layout: Object[]}}
 */
function useLayout(channels) {
    const availHeight = useScreenAvailHeight();

    const {
        cols,
        fullScreenRows,
        layout,
    } = useMemo(() => calcLayout(channels), [channels]);

    return {
        cols,
        rowHeight: Math.floor(availHeight / fullScreenRows),
        layout,
    };
}

function calcLayout(channels) {
    // Listing all scenes
    const scenes = Object
        .entries(
            channels
                .reduce((acc, { channel, scene }) => {
                    if (!acc[scene]) {
                        acc[scene] = [];
                    }

                    acc[scene].push(channel.id);

                    return acc;
                }, {}),
        )
        .map(([scene, listId]) => [scene, listId])

        // Sorting all scenes
        .sort(([a], [b]) => (a > b ? -1 : 1))

        // All items of the list are items on the same scene
        .map(([, listId]) => listId);

    const layoutType = scenes.length > 1 && scenes[0].length <= 2
        ? 'presentation'
        : 'mosaic';

    let layout;
    const gridCols = 12;
    const fullScreenRows = 5;

    if (layoutType === 'presentation') {
        const level1Rows = Math.floor(fullScreenRows / 2);
        const level2Rows = level1Rows / 2;

        layout = [
            ...scenes[0].map((i, index) => ({
                i: i.toString(),
                x: index,
                y: 0,
                w: gridCols - (layout[0].length * (gridCols / 3)),
                h: fullScreenRows,
            })),

            ...scenes[1].map((i, index) => ({
                i: i.toString(),
                x: (gridCols / 3) * 2,
                y: level1Rows * index,
                w: gridCols / 3,
                h: level1Rows,
            })),

            ...scenes.slice(2).map((i, index) => ({
                i: i.toString(),
                x: (gridCols / 3) * 2,
                y: scenes[1].length * 2 + index,
                w: gridCols / 3,
                h: level2Rows,
            })),
        ];
    }
    else {
        const defaultAreas = 2;
        const areas = [8, 4, 2];
        layout = scenes.reduce(
            (acc, list, index) => [
                ...acc,
                ...list.map((id) => ({
                    i: id.toString(),
                    w: index < areas.length ? areas[index] : defaultAreas,
                    h: index < areas.length ? areas[index] / 2 : defaultAreas / 2,
                    x: 0,
                    y: 0,
                })),
            ],
            [],
        );
    }

    return {
        cols: gridCols,
        fullScreenRows,
        layout,
    };
}

// ============================================================
// Exports
export default useLayout;
