// ============================================================
// Import packages
import React, { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

// ============================================================
// Import modules
import useApp from './useApp';
import useChannelCreation from './useChannelCreation';
import CreateChannelDropdown from './CreateChannelDropdown';
import ChannelList from './ChannelList';
import View from './View';
import { map as AppMap } from '../apps';
import { useInnerHeight } from '../hooks';
import { SocketConnectionContext } from '../contexts';

// ============================================================
// Scene

function App({
    wsurl,
}) {
    const [title, setTitle] = useState();
    const [presenter, setPresenter] = useState(false);
    const innerHeight = useInnerHeight();
    const navBarRef = useRef();

    const availHeight = navBarRef.current
        ? innerHeight - navBarRef.current.offsetHeight - 5
        : undefined;

    const {
        focusView,
        focusedViewId,
        socketConnection,
        views,
    } = useApp(wsurl, presenter);

    const {
        cancelChannelCreation,
        channelTypeCreated,
        createChannel,
        startChannelCreation,
    } = useChannelCreation(socketConnection);

    const onPointerToggle = useCallback(({ currentTarget }) => {
        const enabled = currentTarget.checked;
        socketConnection.togglePointer(enabled);
    }, [socketConnection]);

    const onPresenterToggle = useCallback(({ currentTarget }) => {
        const enabled = currentTarget.checked;
        socketConnection.togglePresenter(enabled);
        setPresenter(enabled);
    }, [socketConnection]);


    // ==============================
    // List views
    const viewList = views.map(({ id, channel }) => {
        const focused = id === focusedViewId;
        return (
            <View
                key={channel.id}
                availHeight={availHeight}
                channel={channel}
                focused={focused}
                setTitle={setTitle}
            />
        );
    });

    // ==============================
    // Create view
    let createView;

    if (channelTypeCreated) {
        const Component = AppMap[channelTypeCreated].Create;
        createView = <Component onCreate={createChannel} onCancel={cancelChannelCreation} />;
    }

    const createClass = channelTypeCreated ? 'create' : '';
    const presenterClass = presenter ? 'presenter' : '';

    // ==============================
    // Renderer
    return (
        <Container fluid className={presenterClass}>
            <Row>
                <Navbar ref={navBarRef}>
                    <Navbar.Brand>TTY Stream</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse>
                        <Nav className="mr-auto">
                            <CreateChannelDropdown
                                onCreate={startChannelCreation}
                                creatingChannel={channelTypeCreated}
                            />
                            <ChannelList
                                views={views}
                                setFocusView={focusView}
                                focusedViewId={focusedViewId}
                                readOnly
                            />
                        </Nav>
                        <Form inline>
                            <Form.Check
                                type="switch"
                                id="presenter"
                                label="Présentateur"
                                onChange={onPresenterToggle}
                            />
                            <Form.Check
                                type="switch"
                                id="share-cusor"
                                label="Partager le pointeur"
                                onChange={onPointerToggle}
                                disabled={!presenter}
                            />
                        </Form>
                        <Navbar.Text>{title}</Navbar.Text>
                    </Navbar.Collapse>
                </Navbar>
            </Row>
            <Row className={`app ${createClass}`}>
                {createView}
                <SocketConnectionContext.Provider value={socketConnection}>
                    {viewList}
                </SocketConnectionContext.Provider>
            </Row>
        </Container>
    );
}

App.propTypes = {
    wsurl: PropTypes.string.isRequired,
};

// ============================================================
// Exports
export default App;
