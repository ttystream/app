// ============================================================
// Import packages
import { useState, useCallback } from 'react';

// ============================================================
// Hook
/**
 * @callback CreateChannel
 * @param {ChannelType} type
 */

/**
  * @callback StartChannelCreation
  * @param {ChannelType} type
  */

/**
 * @typedef {Object} UseChannelCreationReturn
 * @property {ChannelType}          channelTypeCreated
 * @property {CreateChannel}        createChannel
 * @property {function}             cancelChannelCreation
 * @property {StartChannelCreation} startChannelCreation
 */

/**
 *
 * @param {SocketConnection} socketConnection
 * @returns {UseChannelCreationReturn}
 */
function useChannelCreation(socketConnection) {
    // Type of channel create.
    // Undefined if no channel currently created
    const [channelTypeCreated, setCreateChanType] = useState();

    /**
     * Callback starting the channel creation
     * @type StartChannelCreation
     */
    const startChannelCreation = useCallback((channelType) => {
        setCreateChanType(channelType);
    }, []);

    // Callback cancelling the channel creation
    const cancelChannelCreation = useCallback(() => {
        setCreateChanType();
    }, []);

    /**
     * Perform the channel creation
     * @type CreateChannel
     */
    const createChannel = useCallback((initialState) => {
        socketConnection.createChannel(
            channelTypeCreated,
            initialState,
        );

        cancelChannelCreation();
    }, [cancelChannelCreation, channelTypeCreated, socketConnection]);

    return {
        channelTypeCreated,
        createChannel,
        cancelChannelCreation,
        startChannelCreation,
    };
}

// ============================================================
// Exports
export default useChannelCreation;
