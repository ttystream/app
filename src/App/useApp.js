// ============================================================
// Import packages
import {
    useMemo,
    useRef,
    useState,
} from 'react';

// ============================================================
// Import modules
import useSocketConnection from './useSocketConnection';

// ============================================================
// Hook

/**
 * @typedef {Object} UseAppReturn
 *
 * @param {function(id: number)} displayView   - Set the view to display
 * @param {function(id: number)} focusView     - Focus a view
 * @param {Pointer}              pointer       - Position of the pointer
 * @param {number}               viewDisplayed - View currently displayed
 * @param {number}               viewFocused   - View currently focused
 * @param {Object[]}             views         - List of all views
 * @param {Channel}              views.channel - Channel providing data for the view
 * @param {number}               views.id      - ID of the view
 * @param {number}               views.title   - Title of the view
 * @param {string}               views.type    - Type of the view
 */

/**
 *
 * @param {string} wsurl
 * @param {boolean} presenter
 * @returns {UseAppReturn}
 */
function useApp(wsurl, presenter) {
    const pointerBoard = useRef();

    const {
        channels,
        focusedChannelId,
        focusChannel,
        pointer,
        socketConnection,
    } = useSocketConnection(wsurl, presenter);

    const [displayedViewId, setDisplayedView] = useState(focusedChannelId); // ID of the current view

    if (displayedViewId === undefined && focusedChannelId) {
        setDisplayedView(focusedChannelId);
    }

    const views = useMemo(
        () => Object
            .values(channels)
            .sort((a, b) => {
                if (a.id > b.id) {
                    return 1;
                }

                return -1;
            })
            .map((channel) => ({
                channel,
                id: channel.id,
                title: channel.title,
                type: channel.type,
            })),
        [channels],
    );

    return {
        displayView: setDisplayedView,
        focusView: focusChannel,
        pointer,
        displayedViewId,
        focusedViewId: focusedChannelId,
        socketConnection,
        pointerBoard,
        views,
    };
}


// ============================================================
// Exports
export default useApp;
