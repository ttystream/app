// ============================================================
// Import packages
import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import NavDropdown from 'react-bootstrap/NavDropdown';

// ============================================================
// Import components
import Channel from '../Channel';

// ============================================================
// Component
function ChannelList({
    views,
    setFocusView,
    focusedViewId,
}) {
    const onSelect = useCallback((viewIdStr) => {
        const viewId = Number(viewIdStr);
        setFocusView(viewId);
    }, [setFocusView]);

    const focusedView = focusedViewId !== undefined
        ? views.find(({ id }) => id === focusedViewId)
        : undefined;

    const title = focusedView
        ? `${focusedView.type}. ${focusedView.id}`
        : 'No views';

    const list = views.map((view) => (
        <NavDropdown.Item
            key={view.id}
            href="#"
            eventKey={view.id}
            onSelect={onSelect}
            active={view.id === focusedViewId}
        >
            {view.id}
            /
            {view.type}
        </NavDropdown.Item>
    ));

    return (
        <NavDropdown title={title}>
            {list}
        </NavDropdown>
    );
}

ChannelList.defaultProps = {
    focusedViewId: undefined,
};

ChannelList.propTypes = {
    views: PropTypes.arrayOf(
        PropTypes.shape({
            channel: PropTypes.instanceOf(Channel),
            title: PropTypes.string,
            type: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
        }).isRequired,
    ).isRequired,

    setFocusView: PropTypes.func.isRequired,
    focusedViewId: PropTypes.number,
};

// ============================================================
// Exports
export default ChannelList;
