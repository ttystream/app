// ============================================================
// Import packages
import {
    useCallback,
    useEffect,
    useRef,
    useState,
} from 'react';

import SocketConnection, { SocketConnectionEvent } from '../SocketConnection';

// ============================================================
// Hook
function useSocketConnection(wsurl, presenter) {
    const [channels, setChannels] = useState({}); // List of all channels
    const [socketConnection, setSocketConnection] = useState(); // Socket connection object
    const [focusedChannelId, setChannelFocus] = useState(); // Id of the currently focused channel
    const [pointer, setPointer] = useState([0, 0]); // // Pointer information
    const ref = useRef(); // Reference to the current channel list

    ref.current = channels;

    // Create the Socket Connection
    useEffect(() => {
        const socketServer = new SocketConnection(wsurl, presenter);
        setSocketConnection(socketServer);

        const newChannelListeners = onNewChannel.bind(undefined, ref, setChannels);
        const deleteChannelListener = onDeleteChannel.bind(undefined, ref, setChannels);
        const focusedChannelListener = onChannelFocused.bind(undefined, setChannelFocus);
        const pointerDisabledListener = onPointerDisabled.bind(undefined, setPointer);
        const pointerMoveListener = onPointerMove.bind(undefined, setPointer);

        // Event: focus channel
        socketServer.addEventListener(SocketConnectionEvent.focus, focusedChannelListener);

        // Event: new channel
        socketServer.addEventListener(SocketConnectionEvent.newChannel, newChannelListeners);

        // Event: remove channel
        socketServer.addEventListener(SocketConnectionEvent.deleteChannel, deleteChannelListener);

        // Event: pointer move
        socketServer.addEventListener(SocketConnectionEvent.pointerMove, pointerMoveListener);

        // Event: pointer disabled
        socketServer.addEventListener(SocketConnectionEvent.pointerDisabled, pointerDisabledListener);

        return () => {
            socketServer.removeEventListener(SocketConnectionEvent.focus, (focusedChannelListener));
            socketServer.removeEventListener(SocketConnectionEvent.newChannel, newChannelListeners);
            socketServer.removeEventListener(SocketConnectionEvent.removeChannel, deleteChannelListener);
            socketServer.removeEventListener(SocketConnectionEvent.pointerMove, pointerMoveListener);
            socketServer.removeEventListener(SocketConnectionEvent.pointerDisabled, pointerDisabledListener);
        };
    }, [
        wsurl,
        presenter,
    ]);

    const focusChannel = useCallback((channelId) => {
        socketConnection.setFocusedChannel(channelId);
        setChannelFocus(channelId);
    }, [socketConnection]);

    return {
        channels,
        focusedChannelId,
        focusChannel,
        pointer,
        socketConnection,
    };
}

/**
 *
 * @param {function}           setFocusedChannel
 * @param {{channel: Channel}} detail
 */
function onChannelFocused(setFocusedChannel, { detail }) {
    setFocusedChannel(detail.channel ? detail.channel.id : undefined);
}


/**
 *
 * @param {Map.<{channel: Channel}>} channels
 * @param {Channel}    channel       - Created channel
 */
function onNewChannel(refChannel, setChannels, { detail }) {
    const { channel } = detail;

    const channels = { ...refChannel.current };

    channels[channel.id] = channel;

    // eslint-disable-next-line no-param-reassign
    refChannel.current = channels;
    setChannels(channels);
}

/**
 *
 * @param {function} setPointer
 */
function onPointerDisabled(setPointer) {
    setPointer();
}

/**
 * @param {function} setPointer
 * @param {number}   x
 * @param {number}   y
 * @param {Channel}  channel
 */
function onPointerMove(setPointer, { detail: { x, y, channel } }) {
    setPointer({ x, y, channel });
}

/**
 *
 * @param {Map.<{channel: Channel, initialState: object, scene: number}>} channels
 * @param {Channel}    channel - The channel that has been moved
 */
function onDeleteChannel(refChannel, setChannels, { detail }) {
    const { channel } = detail;
    const channels = { ...refChannel.current };

    delete channels[channel.id];

    // eslint-disable-next-line no-param-reassign
    refChannel.current = channels;
    setChannels(channels);
}

// ============================================================
// Exports
export default useSocketConnection;
