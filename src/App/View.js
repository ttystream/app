// ============================================================
// Import packages
import React from 'react';
import PropTypes from 'prop-types';

// ============================================================
// Import modules
import Channel from '../Channel';
import { map as AppMap } from '../apps';

// ============================================================
// Component

function View({
    availHeight,
    channel,
    focused,
    setTitle,
}) {
    const Component = AppMap[channel.type].App;
    const focusClass = focused ? 'focus' : '';

    return (
        <div className={`view ${focusClass}`}>
            <div>{channel.title}</div>
            <div className="view-content">
                <Component
                    channel={channel}
                    availHeight={availHeight}
                    setTitle={setTitle}
                />
            </div>
        </div>
    );
}

View.defaultProps = {
    availHeight: undefined,
};

View.propTypes = {
    availHeight: PropTypes.number,
    channel: PropTypes.instanceOf(Channel).isRequired,
    focused: PropTypes.bool.isRequired,
    setTitle: PropTypes.func.isRequired,
};

// ============================================================
// Exports
export default View;
