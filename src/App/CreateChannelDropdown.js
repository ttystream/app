// ============================================================
// Import packages
import React from 'react';
import PropTypes from 'prop-types';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import { map as AppMap } from '../apps';

const listApps = Object.keys(AppMap);

// ============================================================
// Component
function CreateChannelDropdown({
    creatingChannel,
    onCreate,
}) {
    const buttonLabel = creatingChannel || 'Ajouter...';

    const title = (
        <>
            <FontAwesomeIcon icon={faPlus} />
            {buttonLabel}
        </>
    );

    const list = listApps.map((name) => (
        <NavDropdown.Item
            key={name}
            href="#"
            eventKey={name}
            onSelect={onCreate}
        >
            {name}
        </NavDropdown.Item>
    ));

    return (
        <NavDropdown title={title}>
            {list}
        </NavDropdown>
    );
}

CreateChannelDropdown.defaultProps = {
    creatingChannel: undefined,
};

CreateChannelDropdown.propTypes = {
    creatingChannel: PropTypes.string,
    onCreate: PropTypes.func.isRequired,
};

// ============================================================
// Exports
export default CreateChannelDropdown;
