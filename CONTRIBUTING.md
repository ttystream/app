# TTYStream development

## Pre-requisites
* NodeJS v10+

## Install

```
git clone git@gitlab.com:ttystream/app.git
cd app
npm ci
```

## Running in development mode


```
npm run dev
```

This will start a server on 8080 port. The websocket server is expected to be 5000 (configured in `webpack.development.js` file).
